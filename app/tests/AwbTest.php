<?php

class AwbTest extends TestCase {

	public function test_home_route()
	{
		$this->call('GET', '/');
		$this->assertResponseOk();
	}

	public function test_route_with_all_awb_codes()
	{
		$this->call('GET', 'awb');
		$this->assertResponseOk();
	}

	public function test_route_with_one_awb_code()
	{
		$this->call('GET', 'awb');
		$this->assertResponseOk();
	}
}