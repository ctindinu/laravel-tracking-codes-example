<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AWB tracking</title>
	@yield('styles')
</head>
<body>
	<div class="content">
		@yield('content')
	</div>
	{{HTML::script('https://code.jquery.com/jquery-2.1.1.min.js')}}
	@yield('scripts')
</body>
</html>