<?php
namespace Acme\Repositories;
use AwbCode, Response;

class SQLiteAwbRepository implements AwbRepositoryInterface{

	public function getAll()
	{
		return Response::json(AwbCode::all());
	}

	public function find($code)
	{
		return Response::json(AwbCode::whereCode($code)->first());
	}

}