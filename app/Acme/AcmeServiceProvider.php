<?php
namespace Acme;

use Illuminate\Support\ServiceProvider, Config;

class AcmeServiceProvider extends ServiceProvider{

	private static $bindings = [
		'sqlite' => ['interface' => 'Acme\Repositories\AwbRepositoryInterface', 'dependency' => 'Acme\Repositories\SQLiteAwbrepository'],
		'csv' => ['interface' => 'Acme\Repositories\AwbRepositoryInterface', 'dependency' => 'Acme\Repositories\CSVAwbRepository'],
	];

	public function register(){
		$binding = self::$bindings[Config::get('acme.db_storage')];
		$this->app->bind($binding['interface'], $binding['dependency']);
	}
}