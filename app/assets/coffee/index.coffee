$ ->

	$('#btn-search').on 'click', ()->
		url = "/awb/#{$('#search').val()}"
		request = $.get url
		request.done (data)->
			if(!$.isArray(data))
				$('.deliver-msg').html "<p>You package will be deliver due date:<br> #{data.deliver}</p>"
			if($.isArray(data))
				$('.deliver-msg').html "<p>Please enter a code.</p>"
			if($.isEmptyObject(data))
				$('.deliver-msg').html "<p>The code doesn't exists in our database.<p>"