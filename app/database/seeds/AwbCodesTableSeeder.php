<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AwbCodesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			AwbCode::create([
				'code' => $faker->randomNumber(13),
				'deliver' => $faker->dateTimeBetween('now', '+3 days')
			]);
		}
	}

}