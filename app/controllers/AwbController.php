<?php
use Acme\Repositories\AwbRepositoryInterface;

class AwbController extends \BaseController {

	public function __construct(AwbRepositoryInterface $awb)
	{
		$this->awb = $awb;
	}

	public function index()
	{
		return $this->awb->getAll();
	}

	public function show($code)
	{
		return $this->awb->find($code);
	}

}