(function() {
  $(function() {
    return $('#btn-search').on('click', function() {
      var request, url;
      url = "/awb/" + ($('#search').val());
      request = $.get(url);
      return request.done(function(data) {
        if (!$.isArray(data)) {
          $('.deliver-msg').html("<p>You package will be deliver due date:<br> " + data.deliver + "</p>");
        }
        if ($.isArray(data)) {
          $('.deliver-msg').html("<p>Please enter a code.</p>");
        }
        if ($.isEmptyObject(data)) {
          return $('.deliver-msg').html("<p>The code doesn't exists in our database.<p>");
        }
      });
    });
  });

}).call(this);
